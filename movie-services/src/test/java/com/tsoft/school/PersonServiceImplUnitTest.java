package com.tsoft.school;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.junit.MockitoJUnitRunner;

import com.tsoft.school.model.Person;
import com.tsoft.school.model.exception.ResourceNotFoundException;
import com.tsoft.school.repository.PersonRepository;
import com.tsoft.school.service.PersonService;
import com.tsoft.school.service.impl.PersonServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceImplUnitTest {

	@Mock
	private PersonRepository personRepository;

	private PersonService personService;

	@Before
	public void init() {
		this.personService = new PersonServiceImpl(this.personRepository);
	}

	@Test
	public void correctlyFindAllMovies() {
		// Seteo de las precondiciones
		List<Person> peopleInCatalog = Arrays.asList(new Person("Naomi", "Watts", LocalDate.of(1970, 6, 6)),
				new Person("Al", "Pacino", LocalDate.of(1930, 6, 6)));
		when(personRepository.findAll()).thenReturn(peopleInCatalog);

		// Ejecución de la prueba
		List<Person> peopleRetrieved = this.personService.findAll();

		// Verificación de las expectativas
		assertThat("La lista devuelta por el servicio es incorrecta", peopleRetrieved, equalTo(peopleInCatalog));
		verify(this.personRepository, times(1)).findAll();
	}

	@Test
	public void correctlyFindOneMovieById() throws ResourceNotFoundException {
		String personId = UUID.randomUUID().toString();
		// Seteo de las precondiciones
		Person onlyOnePerson = new Person("Naomi", "Watts", LocalDate.of(1970, 6, 6));
		when(personRepository.findById(eq(personId))).thenReturn(Optional.of(onlyOnePerson));

		// Ejecución de la prueba
		Person personRetrieved = this.personService.findById(personId);

		// Verificación de las expectativas
		assertThat("La persona devuelta por el servicio es incorrecta", personRetrieved, equalTo((onlyOnePerson)));
		verify(this.personRepository, times(1)).findById(eq(personId));
	}

}
