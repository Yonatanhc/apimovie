package com.tsoft.school;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.junit.MockitoJUnitRunner;

import com.tsoft.school.model.Movie;
import com.tsoft.school.model.exception.ResourceNotFoundException;
import com.tsoft.school.repository.MovieRepository;
import com.tsoft.school.service.MovieService;
import com.tsoft.school.service.impl.MovieServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceImplUnitTest {

	@Mock
	private MovieRepository movieRepository;

	private MovieService movieService;

	@Before
	public void init() {
		this.movieService = new MovieServiceImpl(this.movieRepository);
	}

	@Test
	public void correctlyFindAllMovies() {
		// Seteo de las precondiciones
		List<Movie> moviesInCatalog = Arrays.asList(new Movie("La llamada", 2002), new Movie("Titanic", 1997));
		when(movieRepository.findAll()).thenReturn(moviesInCatalog);

		// Ejecución de la prueba
		List<Movie> moviesRetrieved = this.movieService.findAll();

		// Verificación de las expectativas
		assertThat("La lista devuelta por el servicio es incorrecta", moviesRetrieved, equalTo(moviesInCatalog));
		verify(this.movieRepository, times(1)).findAll();
	}

	@Test
	public void correctlyFindOneMovieById() throws ResourceNotFoundException {
		String movieId = UUID.randomUUID().toString();
		// Seteo de las precondiciones
		Movie onlyOneMovie = new Movie("La llamada", 2002);
		when(movieRepository.findById(eq(movieId))).thenReturn(Optional.of(onlyOneMovie));

		// Ejecución de la prueba
		Movie movieRetrieved = this.movieService.findById(movieId);

		// Verificación de las expectativas
		assertThat("La película devuelta por el servicio es incorrecta", movieRetrieved, equalTo((onlyOneMovie)));
		verify(this.movieRepository, times(1)).findById(eq(movieId));
	}

}
