package com.tsoft.school.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsoft.school.model.Person;
import com.tsoft.school.model.exception.ResourceNotFoundException;
import com.tsoft.school.repository.PersonRepository;
import com.tsoft.school.service.PersonService;

/**
 * Implementación del servicio de personas
 * 
 * @author TSOFT
 *
 */
@Service
public class PersonServiceImpl implements PersonService {

	private PersonRepository personRepository;

	public PersonServiceImpl(@Autowired PersonRepository actorRepository) {
		this.personRepository = actorRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Person> findAll() {
		return this.personRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Person findById(String uuid) throws ResourceNotFoundException {
		Optional<Person> actor = this.personRepository.findById(uuid);
		if (actor.isPresent()) {
			return actor.get();
		}
		throw new ResourceNotFoundException("No se encontró el actor " + uuid);
	}

}
