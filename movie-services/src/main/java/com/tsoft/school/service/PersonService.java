package com.tsoft.school.service;

import java.util.List;

import com.tsoft.school.model.Person;
import com.tsoft.school.model.exception.ResourceNotFoundException;

/**
 * Interfaz del servicio de personas
 * 
 * @author TSOFT
 *
 */
public interface PersonService {

	List<Person> findAll();

	Person findById(String uuid) throws ResourceNotFoundException;

}
