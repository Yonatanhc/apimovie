package com.tsoft.school.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.tsoft.school.model.Movie;
import com.tsoft.school.model.exception.ResourceNotFoundException;
import com.tsoft.school.repository.MovieRepository;
import com.tsoft.school.service.MovieService;

/**
 * Implementación del servicio de películas
 * 
 * @author TSOFT
 *
 */
@Service
public class MovieServiceImpl implements MovieService {

	private MovieRepository movieRepository;

	public MovieServiceImpl(@Autowired MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Movie> findAll() {
		return this.movieRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Movie findById(String uuid) throws ResourceNotFoundException {
		Optional<Movie> movie = this.movieRepository.findById(uuid);
		if (movie.isPresent()) {
			return movie.get();
		}
		throw new ResourceNotFoundException("No se encontró la película " + uuid);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void update(Movie movie) throws ResourceNotFoundException {
		Movie movieToUpdate = this.movieRepository.findById(movie.getId()).orElseThrow(
				() -> new ResourceNotFoundException("No se encontró la película para actualizar" + movie.getId()));

		if (!StringUtils.isEmpty(movie.getName())) {
			movieToUpdate.setName(movie.getName());
		}

		if (movie.getYear() > 0) {
			movieToUpdate.setYear(movie.getYear());
		}

		this.movieRepository.save(movieToUpdate);
	}

}
