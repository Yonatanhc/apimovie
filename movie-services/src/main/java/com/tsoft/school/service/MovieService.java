package com.tsoft.school.service;

import java.util.List;

import com.tsoft.school.model.Movie;
import com.tsoft.school.model.exception.ResourceNotFoundException;

/**
 * Interfaz del servicio de películas
 * 
 * @author TSOFT
 *
 */
public interface MovieService {

	List<Movie> findAll();

	Movie findById(String uuid) throws ResourceNotFoundException;

	void update(Movie movie) throws ResourceNotFoundException;

}
