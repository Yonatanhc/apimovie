package com.tsoft.school.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsoft.school.dto.PersonDto;
import com.tsoft.school.dto.DtoAssembler;
import com.tsoft.school.model.Person;
import com.tsoft.school.model.exception.ResourceNotFoundException;
import com.tsoft.school.service.PersonService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller de personas. Expone los recursos del API para operar con el
 * catálogo.
 *
 * @author TSOFT
 */
@RestController
@RequestMapping("/api/v1")
public class PersonController {

	/**
	 * Se inyecta el servicio de personas.
	 */
	@Autowired
	private PersonService personService;

	/**
	 * Se inyecta el ensamblador que convierte el objeto de dominio que proviene del
	 * servicio de personas, en el objeto que se expone en el API.
	 */
	@Autowired
	private DtoAssembler dtoAssembler;

	/**
	 * Obtiene todas los personas disponibles en el sistema.
	 *
	 * @return la lista completa de personas.
	 */
	@GetMapping("/people")
	public List<PersonDto> getAll() {
		return this.personService.findAll().stream().map((Person actor) -> this.dtoAssembler.asPersonDto(actor))
				.collect(Collectors.toList());
	}

	/**
	 * Obtiene una persona por su id.
	 *
	 * @return la persona
	 * @throws ResourceNotFoundException
	 */
	@GetMapping("/people/:id")
	public PersonDto get(String id) throws ResourceNotFoundException {
		return this.dtoAssembler.asPersonDto(this.personService.findById(id));
	}
}
