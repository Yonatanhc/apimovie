package com.tsoft.school.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tsoft.school.dto.DtoAssembler;
import com.tsoft.school.dto.MovieDto;
import com.tsoft.school.dto.UpdateMovieDto;
import com.tsoft.school.model.Movie;
import com.tsoft.school.model.exception.ResourceNotFoundException;
import com.tsoft.school.service.MovieService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller de películas. Expone los recursos del API para operar con el
 * catálogo.
 *
 * @author TSOFT
 */
@RestController
@RequestMapping("/api/v1")
public class MovieController {

	/**
	 * Se inyecta el servicio de películas.
	 */
	@Autowired
	private MovieService movieService;

	/**
	 * Se inyecta el ensamblador que convierte el objeto de dominio que proviene del
	 * servicio de películas, en el objeto que se expone en el API.
	 */
	@Autowired
	private DtoAssembler dtoAssembler;

	/**
	 * Obtiene todas las películas existentes en el catálogo.
	 *
	 * @return la lista completa de películas.
	 */
	@GetMapping("/movies")
	public List<MovieDto> getAll() {
		return this.movieService.findAll().stream().map((Movie movie) -> this.dtoAssembler.asMovieDto(movie))
				.collect(Collectors.toList());
	}

	/**
	 * Obtiene una película por su id.
	 *
	 * @return la película
	 * @throws ResourceNotFoundException
	 */
	@GetMapping("/movies/{id}")
	public MovieDto get(@PathVariable String id) throws ResourceNotFoundException {
		return this.dtoAssembler.asMovieDto(this.movieService.findById(id));
	}

	/**
	 * Obtiene una película por su id.
	 *
	 * @return la película
	 * @throws ResourceNotFoundException
	 */
	@PutMapping("/movies/{id}")
	public void update(@PathVariable String id, @RequestBody UpdateMovieDto updateData)
			throws ResourceNotFoundException {
		this.movieService.update(this.dtoAssembler.asMovie(id, updateData));
	}
}
