package com.tsoft.school.dto;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tsoft.school.model.Person;
import com.tsoft.school.model.Movie;

/**
 * Transforma objetos de dominio en objetos del API.
 * 
 * @author TSOFT
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DtoAssembler {

	public Movie asMovie(String id, UpdateMovieDto updateMovie) {
		Movie movie = new Movie(id);

		movie.setName(updateMovie.getName());
		movie.setYear(updateMovie.getYear());

		return movie;
	}

	public MovieDto asMovieDto(Movie movie) {
		MovieDto movieDto = new MovieDto();

		movieDto.setId(movie.getId());
		movieDto.setName(movie.getName());
		movieDto.setYear(movie.getYear());

		movieDto.setCast(
				movie.getCast().stream().map((Person actor) -> this.asPersonDto(actor)).collect(Collectors.toSet()));

		movieDto.setCreatedAt(movie.getCreatedAt());
		movieDto.setCreatedBy(movie.getCreatedBy());
		movieDto.setUpdatedAt(movie.getUpdatedAt());
		movieDto.setUpdatedBy(movie.getUpdatedBy());
		movieDto.setVersion(movie.getVersion());

		return movieDto;
	}

	public Optional<MovieDto> asMovieDto(Optional<Movie> optionalMovie) {
		if (optionalMovie.isPresent()) {
			MovieDto movieDto = this.asMovieDto(optionalMovie.get());
			return Optional.of(movieDto);
		}
		return Optional.empty();
	}

	public PersonDto asPersonDto(Person person) {
		PersonDto personDto = new PersonDto();

		personDto.setId(person.getId());
		personDto.setName(person.getName());
		personDto.setSurname(person.getSurname());
		personDto.setBirthDate(person.getBirthDate());
		personDto.setCreatedAt(person.getCreatedAt());
		personDto.setCreatedBy(person.getCreatedBy());
		personDto.setUpdatedAt(person.getUpdatedAt());
		personDto.setUpdatedBy(person.getUpdatedBy());
		personDto.setVersion(person.getVersion());

		return personDto;
	}

	public Optional<PersonDto> asPersonDto(Optional<Person> optionalPerson) {
		if (optionalPerson.isPresent()) {
			PersonDto personDto = this.asPersonDto(optionalPerson.get());
			return Optional.of(personDto);
		}
		return Optional.empty();
	}
}
