package com.tsoft.school.dto;

/**
 * Datos para un update de una película.
 * 
 * @author TSOFT
 *
 */
public class UpdateMovieDto {

	private String name;
	private int year;
	// private Set<PersonDto> cast;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

}
