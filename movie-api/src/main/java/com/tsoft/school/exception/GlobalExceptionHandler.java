package com.tsoft.school.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.tsoft.school.model.exception.ResourceNotFoundException;

import java.util.Date;

/**
 * Exception handler global que mapea errores a status de response del API.
 *
 * @author TSOFT
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * Resource not found exception response entity.
	 *
	 * @param ex      the ex
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
		return new ResponseEntity<>(this.errorResponseFor(HttpStatus.NOT_FOUND, ex, request), HttpStatus.NOT_FOUND);
	}

	/**
	 * Global exception handler response entity.
	 *
	 * @param ex      the ex
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> globalExceptionHandler(Exception ex, WebRequest request) {
		return new ResponseEntity<>(this.errorResponseFor(HttpStatus.INTERNAL_SERVER_ERROR, ex, request),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ErrorResponse errorResponseFor(HttpStatus status, Exception ex, WebRequest request) {
		return new ErrorResponse(new Date(), status.toString(), ex.getMessage(), request.getDescription(false));
	}
}
