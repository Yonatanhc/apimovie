package com.tsoft.school;

import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Entry point de la aplicación Catálogo de películas
 *
 * @author TSOFT
 */
@SpringBootApplication(scanBasePackages = "com.tsoft.school")
@EnableJpaAuditing
public class Application {

	/**
	 * Punto de entrada de la aplicación
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Devuelve el bean que administrará auditoria (usuario de creación y update de
	 * entidades).
	 * 
	 * @return implementación del bean de auditoría que espera Spring.
	 */
	@Bean
	public AuditorAware<String> auditorProvider() {
		return new AuditorAware<String>() {
			/**
			 * Esta aplicación, dado que no maneja sesiones de usuario, devuelve a "user"
			 * como el usuario involucrado en todas las transacciones.
			 */
			@Override
			public Optional<String> getCurrentAuditor() {
				return Optional.of("user");
			}
		};
	}
}
