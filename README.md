# movie-catalog

Proyecto Spring boot Rest API para el manejo de películas, utilizado para enseñanza en escuela IT TSOFT.

Prerequisitos:
- Java 8
- Maven 3.6+
- PostgreSQL

# Construir el proyecto:

mvn clean package install

# Sólo correr los tests:

mvn test

# Ejecutar el servidor con el API montada:

Moverse al directorio movie-api y ejecutar: mvn spring-boot:run

#El servicio debe quedar disponible en:
http://localhost:7091/api/v1/movies

#La documentación de Swagger debe quedar disponible en:
http://localhost:7091/swagger-ui.html

# Consideraciones:

El api se conecta por default a una instancia de PostgreSQL en localhost, puerto 5432, al esquema público de una base de datos que debe llamarse "movie-catalog".
Toda la conf de acceso a la BD se puede cambiar en movie-api/src/main/resources/application.properties. 