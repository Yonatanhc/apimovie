package com.tsoft.school.model;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

import java.time.LocalDate;
import java.time.Period;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PersonUnitTest {

	@Test
	public void correctlyGetAge() {
		LocalDate birthDate = LocalDate.of(2000, 6, 6);
		int expectedDate = Period.between(birthDate, LocalDate.now()).getYears();

		Person actor = new Person();
		actor.setName("Naomi");
		actor.setSurname("Watts");
		actor.setBirthDate(birthDate);

		assertThat("La edad de la persona fue incorrectamente calculada", actor.getAge(), equalTo(expectedDate));
	}

}
