package com.tsoft.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tsoft.school.model.Person;

/**
 * Interfaz del repositorio de personas.
 *
 * @author TSOFT
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, String> {
}
