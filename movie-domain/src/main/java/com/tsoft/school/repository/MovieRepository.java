package com.tsoft.school.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tsoft.school.model.Movie;

/**
 * Interfaz del repositorio de películas.
 *
 * @author TSOFT
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, String> {}
