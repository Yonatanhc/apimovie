CREATE TABLE IF NOT EXISTS movies (
	id text not null,
	name text not null,
	year int not null,
	created_at timestamp not null,
	created_by text not null,
	updated_at timestamp not null,
	updated_by text not null,
	version bigserial not null,
	primary key (id));
	
CREATE TABLE IF NOT EXISTS people (
	id text not null,
	name text not null,
	surname text not null,
	birth_date DATE not null,
	created_at timestamp not null,
	created_by text not null,
	updated_at timestamp not null,
	updated_by text not null,
	version bigserial not null,
	primary key (id));
	
CREATE TABLE IF NOT EXISTS movies_actors (
	movie_id text not null,
	actor_id text not null,
	primary key (movie_id, actor_id),
	foreign key (movie_id) references movies(id),
	foreign key (actor_id) references people(id));