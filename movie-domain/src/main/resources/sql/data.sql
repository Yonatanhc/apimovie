-- Datos iniciales
insert into movies(id, name, year, created_at, created_by, updated_at, updated_by, version)
	values('670b9562-b30d-52d5-b827-655787665500', 'La llamada', 2002, now(), 'user', now(), 'user', 0);
	
insert into people(id, name, surname, birth_date, created_at, created_by, updated_at, updated_by, version)
	values('670b9562-b30d-52d5-b827-655787665599', 'Naomy', 'Watts', '1968-09-28', now(), 'user', now(), 'user', 0);
	
insert into movies_actors(movie_id, actor_id)
	values('670b9562-b30d-52d5-b827-655787665500', '670b9562-b30d-52d5-b827-655787665599');